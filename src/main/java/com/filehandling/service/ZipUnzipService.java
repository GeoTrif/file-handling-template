package com.filehandling.service;

import static com.filehandling.util.Constants.*;

import com.filehandling.util.ZipUnzipUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class ZipUnzipService {

    public void unzipArchiveFilesDemo() throws IOException {
        ZipUnzipUtil.unzipTarGz(new File(TAR_GZ_FILE_PATH), UNZIPPED_TAR_GZ_FILE_PATH);
        ZipUnzipUtil.unzipGz(new File(GZ_FILE_PATH), UNZIPPED_GZ_FILE_PATH);
        ZipUnzipUtil.unzipZipFile(Paths.get(ZIP_FILE_PATH), Paths.get(UNZIPPED_ZIP_FILE_PATH));
    }

    public void zipFilesDemo() throws IOException {
        ZipUnzipUtil.zipSingleFile(SINGLE_FILE_ZIP_PATH, READ_DEMO_FILE_PATH);
        ZipUnzipUtil.zipMultipleFiles(MULTIPLE_FILES_ZIP_PATH, new File(READ_DEMO_FILE_PATH), new File(MULTIPLE_FILES_DEMO_FILE_PATH));
        ZipUnzipUtil.zipFolder(FOLDER_ZIP_PATH, FOLDER_PATH);
    }
}
