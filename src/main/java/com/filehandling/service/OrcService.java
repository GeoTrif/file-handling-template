package com.filehandling.service;

import static com.filehandling.util.Constants.ORC_FILE_TO_WRITE_PATH;

import com.filehandling.util.OrcUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.orc.OrcFile;
import org.apache.orc.TypeDescription;
import org.apache.orc.Writer;

import java.io.IOException;

public class OrcService {

    private static final TypeDescription ORC_SCHEMA = TypeDescription.fromString("struct<stringValue:string,intValue:int,doubleValue:double>");

    public void writeOrcFileDemo() throws IOException {
        Path orcFilePath = new Path(ORC_FILE_TO_WRITE_PATH);
        Writer writer = OrcFile.createWriter(orcFilePath, OrcFile.writerOptions(new Configuration()).setSchema(ORC_SCHEMA));

        OrcUtil.writeOrcFile(ORC_FILE_TO_WRITE_PATH, writer, ORC_SCHEMA);
    }
}
