package com.filehandling.service;

import static com.filehandling.util.Constants.*;

import com.filehandling.util.CSVUtil;
import com.opencsv.exceptions.CsvException;

import java.io.IOException;

public class CSVService {

    public void readCSVFileDemo() throws IOException, CsvException {
        printDelimiter(READING_CSV_FILE_LINE_BY_LINE_TITLE);
        CSVUtil.readCSVFileLineByLineUsingOpenCSV(CSV_READ_TEST_FILE_PATH);

        printDelimiter(READING_ALL_DATA_FROM_CSV_FILE_TITLE);
        CSVUtil.readAllDataCSVFileUsingOpenCSV(CSV_READ_TEST_FILE_PATH);

        printDelimiter(READING_ALL_DATA_FROM_CSV_FILE_USING_CUSTOM_SEPARATOR_TITLE);
        CSVUtil.readCSVFileWithDifferentSeparatorUsingOpenCSV(CUSTOM_SEPARATOR_CSV_READ_TEST_FILE_PATH);
    }

    public void writeDataToCSVFileDemo() throws IOException {
        CSVUtil.writeDataToCSVFileLineByLineUsingOpenCSV(CSV_WRITE_LINE_BY_LINE_TEST_FILE_PATH);
        CSVUtil.writeAllDataAtOnceToCSVFileUsingOpenCSV(CSV_WRITE_ALL_DATA_TEST_FILE_PATH);
        CSVUtil.writeAllDataAtOnceToCSVFileWithCustomSeparatorUsingOpenCSV(CUSTOM_SEPARATOR_CSV_WRITE_TEST_FILE_PATH);
    }

    private void printDelimiter(final String title) {
        System.out.println(String.format(PRINT_DELIMITER, title));
    }

}
