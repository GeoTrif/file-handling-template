package com.filehandling.service;

import static com.filehandling.util.Constants.*;

import com.filehandling.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class FileHandlingService {

    public void readFileAndFolderDemo(final String filePath, final String folderPath) throws IOException {
        printDelimiter(READ_ALL_BYTES_FROM_FILE_TITLE);
        String readAllBytesFromFileString = FileUtil.readAllBytesFromFile(filePath);
        System.out.println(String.format(READING_ALL_BYTES_FROM_FILE_MESSAGE, readAllBytesFromFileString));

        //TODO Review readFileUsingFileReader() method.
//        FileUtil.readFileUsingFileReader(filePath);
//        printDelimiter();

        printDelimiter(READ_FILE_USING_BUFFERED_READER_TITLE);
        FileUtil.readFileUsingBufferedReader(filePath);

        printDelimiter(READ_FILE_USING_INPUT_STREAM_TITLE);
        FileUtil.readFileUsingFileInputStream(filePath);

        printDelimiter(READ_FILE_USING_APACHE_COMMONS_IO_TITLE);
        List<String> linesFromReadingFileApacheCommonsIO = FileUtil.readFileUsingApacheCommonsIO(new File(filePath));
        printLinesFromList(linesFromReadingFileApacheCommonsIO);

        printDelimiter(READ_FILE_USING_GOOGLE_GUAVA_TITLE);
        List<String> linesFromReadingFileGoogleGuava = FileUtil.readFileUsingGoogleGuava(new File(filePath));
        printLinesFromList(linesFromReadingFileGoogleGuava);

        printDelimiter(PRINT_LINES_FROM_FILE_TITLE);
        List<String> linesFromReadingFile = FileUtil.readAllLinesFromFile(filePath);
        printLinesFromList(linesFromReadingFile);

        printDelimiter(PRINT_FILTERED_LINES_FROM_FILE_TITLE);
        List<String> filteredLinesFromReadingFile = FileUtil.readAndFilterFileLineByLine(new File(filePath));
        printLinesFromList(filteredLinesFromReadingFile);

        printDelimiter(PRINT_FILTERED_LINES_FROM_FILE_USING_REGEX_TITLE);
        List<String> filteredLinesFromReadingFileUsingRegex = FileUtil.readAndFilterFileLineByLineUsingRegex(new File(filePath), REGEX);
        printLinesFromList(filteredLinesFromReadingFileUsingRegex);

        printDelimiter(LIST_ALL_FILES_AND_SUBDIRECTORIES_TITLE);
        FileUtil.listAllFilesAndSubDirectories(folderPath);

        printDelimiter(LIST_ALL_FILES_RECURSIVELY_TITLE);
        FileUtil.listAllFilesRecursively(folderPath);
    }

    public void writeContentToFileDemo() throws IOException {
        FileUtil.writeFile(new File(JAVA_NIO_WRITE_FILE_PATH), TEST_CONTENT);
        FileUtil.writeFileUsingFileWriter(FILE_WRITER_WRITE_FILE_PATH, TEST_CONTENT);
        FileUtil.writeFileUsingBufferedWriter(BUFFERED_WRITER_WRITE_FILE_PATH, TEST_CONTENT);
        FileUtil.writeFileUsingFileOutputStream(FILE_OUTPUT_STREAM_WRITER_WRITE_FILE_PATH, TEST_CONTENT);
        FileUtil.writeFileUsingApacheCommonsIO(new File(APACHE_COMMONS_IO_WRITER_WRITE_FILE_PATH), TEST_CONTENT);
        FileUtil.writeFileUsingGoogleGuava(new File(GOOGLE_GUAVA_WRITER_WRITE_FILE_PATH), TEST_CONTENT);
    }

    // Make sure you create a folder with the folder name used to initialize constant DELETE_FOLDER_PATH.
    public void deleteDirectoryDemo(final String folderPath) throws IOException {
        FileUtil.deleteDirectoryWithFilesAndSubDirectory(new File(folderPath));
    }

    private void printLinesFromList(final List<String> linesList) {
        for (int i = 0; i < linesList.size(); i++) {
            System.out.println(String.format(READING_LINES_FROM_FILE_MESSAGE, (i + 1), linesList.get(i)));
        }
    }

    private void printDelimiter(final String title) {
        System.out.println(String.format(PRINT_DELIMITER, title));
    }
}
