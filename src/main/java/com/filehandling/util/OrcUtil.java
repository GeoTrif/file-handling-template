package com.filehandling.util;

import static com.filehandling.util.Constants.*;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hive.ql.exec.vector.*;
import org.apache.orc.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

public class OrcUtil {

    private static final int MAP_SIZE = 5;
    private static final long INT_COLUMN_CONTENT = 100;
    private static final double DOUBLE_COLUMN_CONTENT = 200.0;
    private static final Map<Integer, String> MAP_COLUMN_CONTENT = new HashMap<>();
    private static final String STRING_BYTES_COLUMN_CONTENT = "String content test";
    private static final Timestamp TIMESTAMP_COLUMN_CONTENT = new Timestamp(new Date().getTime());

    static {
        MAP_COLUMN_CONTENT.put(1, "test1");
        MAP_COLUMN_CONTENT.put(2, "test2");
        MAP_COLUMN_CONTENT.put(3, "test3.txt");
        MAP_COLUMN_CONTENT.put(4, "test4");
        MAP_COLUMN_CONTENT.put(5, "test5");
    }

    private OrcUtil() {

    }

    public static void readOrcFile(final File file, final Reader reader, final TypeDescription schema) throws IOException {
        Configuration conf = new Configuration();
        FileInputStream fis = new FileInputStream(file);
        RecordReader rows = reader.rows();
        VectorizedRowBatch readerBatch = reader.getSchema().createRowBatch();

        while (rows.nextBatch(readerBatch)) {
            BytesColumnVector bytesColumnVector = (BytesColumnVector) readerBatch.cols[0];
            LongColumnVector longColumnVector = (LongColumnVector) readerBatch.cols[1];
            DoubleColumnVector doubleColumnVector = (DoubleColumnVector) readerBatch.cols[2];

            for (int r = 0; r < readerBatch.size; r++) {
                String bytesColumnValue = new String(bytesColumnVector.vector[r]);
                long longColumnValue = (long) longColumnVector.vector[r];
                double doubleColumnValue = doubleColumnVector.vector[r];

                System.out.println(String.format(PRINT_ORC_VALUES, bytesColumnValue, longColumnValue, doubleColumnValue));
            }
        }
    }

    public static void writeOrcFile(final String filePath, final Writer writer, final TypeDescription schema) {
        try {
            VectorizedRowBatch batch = schema.createRowBatch();
            BytesColumnVector bytesColumnVector = (BytesColumnVector) batch.cols[0];
            LongColumnVector intColumnVector = (LongColumnVector) batch.cols[1];
            DoubleColumnVector doubleColumnVector = (DoubleColumnVector) batch.cols[2];

            int row = batch.size++;
            bytesColumnVector.setVal(row, STRING_BYTES_COLUMN_CONTENT.getBytes());
            intColumnVector.vector[row] = INT_COLUMN_CONTENT;
            doubleColumnVector.vector[row] = DOUBLE_COLUMN_CONTENT;

            writer.addRowBatch(batch);
            batch.reset();

        } catch (IOException e) {
            System.out.println(String.format(COULD_NOT_WRITE_ORC_FILE_TO_PATH, filePath));
        }
    }

    public static void writeOrcFile2(final String filePath, final Writer writer, final TypeDescription schema) {
        try {
            VectorizedRowBatch batch = schema.createRowBatch();
            BytesColumnVector bytesColumnVector = (BytesColumnVector) batch.cols[0];
            LongColumnVector longColumnVector = (LongColumnVector) batch.cols[1];
            DoubleColumnVector doubleColumnVector = (DoubleColumnVector) batch.cols[2];
            TimestampColumnVector timestampColumnVector = (TimestampColumnVector) batch.cols[3];

            MapColumnVector mapColumnVector = (MapColumnVector) batch.cols[4];
            LongColumnVector mapKeys = (LongColumnVector) mapColumnVector.keys;
            BytesColumnVector mapValues = (BytesColumnVector) mapColumnVector.values;
            final int batchSize = batch.getMaxSize();
            mapKeys.ensureSize(batchSize * MAP_SIZE, false);
            mapValues.ensureSize(batchSize * MAP_SIZE, false);

            int row = batch.size++;
            bytesColumnVector.setVal(row, STRING_BYTES_COLUMN_CONTENT.getBytes());
            longColumnVector.vector[row] = INT_COLUMN_CONTENT;
            doubleColumnVector.vector[row] = DOUBLE_COLUMN_CONTENT;
            timestampColumnVector.set(row, TIMESTAMP_COLUMN_CONTENT);

            mapColumnVector.offsets[row] = mapColumnVector.childCount;
            mapColumnVector.lengths[row] = MAP_SIZE;
            mapColumnVector.childCount += MAP_SIZE;

            for (int mapElem = (int) mapColumnVector.offsets[row]; mapElem < mapColumnVector.offsets[row] + MAP_SIZE; mapElem++) {
                String value = "row." + (mapElem - mapColumnVector.offsets[row]);
                mapKeys.vector[mapElem] = mapElem;
                mapValues.setVal(mapElem, value.getBytes(StandardCharsets.UTF_8));
            }

            writer.addRowBatch(batch);
            batch.reset();

        } catch (IOException e) {
            System.out.println(String.format(COULD_NOT_WRITE_ORC_FILE_TO_PATH, filePath));
        }
    }
}
