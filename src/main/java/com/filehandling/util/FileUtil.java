package com.filehandling.util;

import static com.filehandling.util.Constants.*;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class FileUtil {

    private FileUtil() {

    }

    /**
     * Read all bytes from a file.
     * Java.nio classes.
     *
     * @param filename
     * @return
     * @throws IOException
     */
    public static String readAllBytesFromFile(String filename) throws IOException {
        return new String(Files.readAllBytes(Paths.get(filename)));
    }

    /**
     * Read all lines from a file.
     * Java.nio classes.
     *
     * @param filename
     * @return
     * @throws IOException
     */
    public static List<String> readAllLinesFromFile(String filename) throws IOException {
        return Files.readAllLines(Paths.get(filename));
    }

    /**
     * Read and filter content of a file line by line.
     * Java.nio  + Java 8.
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static List<String> readAndFilterFileLineByLine(File file) throws IOException {
        return Files.lines(file.toPath())
                .map(line -> line.trim())
                .filter(line -> !line.isEmpty())
                .collect(Collectors.toList());
    }

    /**
     * Read and filter content of a file line by line using a regex.
     * Java.nio  + Java 8.
     *
     * @param file
     * @param yourRegex
     * @return
     * @throws IOException
     */
    public static List<String> readAndFilterFileLineByLineUsingRegex(File file, String yourRegex) throws IOException {
        return Files.lines(file.toPath())
                .map(line -> line.trim())
                .filter(line -> !line.matches(yourRegex))
                .collect(Collectors.toList());
    }

    /**
     * Write file using Java.nio classes.
     *
     * @param file
     * @param content
     * @throws IOException
     */
    public static void writeFile(File file, String content) throws IOException {
        Files.write(file.toPath(), content.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE);
    }

    /**
     * List all files and subDirectories from a folder.
     * Java.nio classes.
     *
     * @param path
     * @throws IOException
     */
    public static void listAllFilesAndSubDirectories(String path) throws IOException {
        Files.list(Paths.get(path)).forEach(System.out::println);
    }

    /**
     * List all files recursively.
     * Java.nio classes.
     *
     * @param path
     * @throws IOException
     */
    public static void listAllFilesRecursively(String path) throws IOException {
        Files.walk(Paths.get(path))
                .filter(Files::isRegularFile)
                .forEach(System.out::println);
    }

    /**
     * Delete a directory with it's files and subDirectories.
     * Java.nio classes.
     *
     * @param file
     * @throws IOException
     */
    public static void deleteDirectoryWithFilesAndSubDirectory(File file) throws IOException {
        Files.walk(file.toPath())
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
    }

    /**
     * Creating a text file using FileWriter.
     *
     * @param filePath
     * @param content
     * @throws IOException
     */
    public static void writeFileUsingFileWriter(String filePath, String content) {
        try (FileWriter fileWriter = new FileWriter(filePath)) {
            fileWriter.write(content);
        } catch (IOException e) {
            System.out.println(String.format(COULD_NOT_WRITE_TO_FILE_ERROR_MESSAGE, filePath));
        }
    }

    /**
     * Reading data from a file using FileReader.
     *
     * @param filePath
     * @throws IOException
     */
    public static void readFileUsingFileReader(String filePath) {
        try (FileReader fileReader = new FileReader(filePath)) {
            int ch = fileReader.read();

            while (ch != -1) {
                System.out.print((char) ch);
            }
        } catch (FileNotFoundException e) {
            System.out.println(String.format(FILE_NOT_FOUND_AT_PATH_ERROR_MESSAGE, filePath));
        } catch (IOException e) {
            System.out.println(String.format(COULD_NOT_READ_FILE_ERROR_MESSAGE, filePath));
        }
    }

    /**
     * Writes the content to a file using BufferedWriter
     *
     * @param filePath
     * @param content
     */
    public static void writeFileUsingBufferedWriter(String filePath, String content) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filePath))) {
            bufferedWriter.write(content);
        } catch (IOException e) {
            System.out.println(String.format(COULD_NOT_WRITE_TO_FILE_ERROR_MESSAGE, filePath));
        }
    }

    /**
     * Reading data from a file using BufferedReader.
     *
     * @param filePath
     */
    public static void readFileUsingBufferedReader(String filePath) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
            String line = bufferedReader.readLine();

            while (line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            System.out.println(String.format(COULD_NOT_READ_FILE_ERROR_MESSAGE, filePath));
        }
    }

    /**
     * Writes the content to a file using FileOutputStream
     *
     * @param filePath
     * @param content
     */
    public static void writeFileUsingFileOutputStream(String filePath, String content) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(filePath)) {
            fileOutputStream.write(content.getBytes());
        } catch (IOException e) {
            System.out.println(String.format(COULD_NOT_WRITE_TO_FILE_ERROR_MESSAGE, filePath));
        }
    }

    /**
     * Reading data from a file using FileInputStream.
     *
     * @param filePath
     * @throws IOException
     */
    public static void readFileUsingFileInputStream(String filePath) {
        try (FileInputStream fileInputStream = new FileInputStream(filePath)) {
            int ch = fileInputStream.read();

            while (ch != -1) {
                System.out.print((char) ch);
                ch = fileInputStream.read();
            }
        } catch (FileNotFoundException e) {
            System.out.println(String.format(FILE_NOT_FOUND_AT_PATH_ERROR_MESSAGE, filePath));
        } catch (IOException e) {
            System.out.println(String.format(COULD_NOT_READ_FILE_ERROR_MESSAGE, filePath));
        }
    }

    /**
     * Write content to a file using Apache CommonsIO library.
     *
     * @param file
     * @param content
     * @throws IOException
     */
    public static void writeFileUsingApacheCommonsIO(File file, String content) throws IOException {
        FileUtils.writeStringToFile(file, content, StandardCharsets.UTF_8);
    }

    /**
     * Read file using Apache CommonsIO library.
     *
     * @param file
     * @throws IOException
     */
    public static List<String> readFileUsingApacheCommonsIO(File file) throws IOException {
        return FileUtils.readLines(file, StandardCharsets.UTF_8);
    }

    /**
     * Write content to a file using Google Guava library.
     *
     * @param file
     * @param content
     * @throws IOException
     */
    public static void writeFileUsingGoogleGuava(File file, String content) throws IOException {
        com.google.common.io.Files.write(content.getBytes(), file);
    }

    /**
     * Read lines from a file using Google Guava library.
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static List<String> readFileUsingGoogleGuava(File file) throws IOException {
        return com.google.common.io.Files.readLines(file, StandardCharsets.UTF_8);
    }
}
