package com.filehandling.util;

import static com.filehandling.util.Constants.*;

import com.opencsv.*;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvValidationException;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVUtil {

    private CSVUtil() {

    }

    private static final List<String[]> DATA = new ArrayList<>();

    static {
        DATA.add(new String[]{"Name", "Class", "Marks"});
        DATA.add(new String[]{"Adam", "10", "90"});
        DATA.add(new String[]{"Roger", "10", "95"});
    }

    /**
     * Read csv file line by line using OpenCSV library.
     *
     * @param csvFilePath
     * @throws IOException
     * @throws CsvValidationException
     */
    public static void readCSVFileLineByLineUsingOpenCSV(final String csvFilePath) throws IOException, CsvValidationException {
        FileReader fileReader = new FileReader(csvFilePath);
        CSVReader csvReader = new CSVReader(fileReader);
        String[] nextRecord;

        while ((nextRecord = csvReader.readNext()) != null) {
            for (String cell : nextRecord) {
                System.out.print(cell + TAB_SEPARATOR);
            }
            System.out.println();
        }
    }

    /**
     * Read all data from a csv file line by line using OpenCSV library.
     *
     * @param csvFilePath
     * @throws IOException
     * @throws CsvException
     */
    public static void readAllDataCSVFileUsingOpenCSV(final String csvFilePath) throws IOException, CsvException {
        FileReader fileReader = new FileReader(csvFilePath);
        CSVReader csvReader = new CSVReaderBuilder(fileReader)
                .withSkipLines(1)
                .build();

        List<String[]> allData = csvReader.readAll();

        for (String[] row : allData) {
            for (String cell : row) {
                System.out.print(cell + TAB_SEPARATOR);
            }
            System.out.println();
        }
    }

    /**
     * Read all data from a csv file with custom separator using OpenCSV library.
     *
     * @param csvFilePath
     * @throws IOException
     * @throws CsvException
     */
    public static void readCSVFileWithDifferentSeparatorUsingOpenCSV(final String csvFilePath) throws IOException, CsvException {
        FileReader fileReader = new FileReader(csvFilePath);
        CSVParser parser = new CSVParserBuilder()
                .withSeparator(READ_CUSTOM_SEPARATOR)
                .build();

        CSVReader csvReader = new CSVReaderBuilder(fileReader)
                .withCSVParser(parser)
                .build();

        List<String[]> allData = csvReader.readAll();

        for (String[] row : allData) {
            for (String cell : row) {
                System.out.print(cell + TAB_SEPARATOR);
            }
            System.out.println();
        }
    }

    /**
     * Write data from a string array to a line in a csv file using OpenCSV library.
     *
     * @param csvFilePath
     * @throws IOException
     */
    public static void writeDataToCSVFileLineByLineUsingOpenCSV(final String csvFilePath) throws IOException {
        File file = new File(csvFilePath);
        FileWriter outputFile = new FileWriter(file);
        CSVWriter writer = new CSVWriter(outputFile);

        String[] header = {"Name", "Class", "Marks"};
        writer.writeNext(header);
        String[] data1 = {"Adam", "10", "90"};
        writer.writeNext(data1);
        String[] data2 = {"Roger", "10", "95"};
        writer.writeNext(data2);

        writer.close();
    }

    /**
     * Write all data in a csv file using OpenCSV library.
     *
     * @param csvFilePath
     * @throws IOException
     */
    public static void writeAllDataAtOnceToCSVFileUsingOpenCSV(final String csvFilePath) throws IOException {
        File file = new File(csvFilePath);
        FileWriter outputFile = new FileWriter(file);
        CSVWriter writer = new CSVWriter(outputFile);

        writer.writeAll(DATA);
        writer.close();
    }

    /**
     * Write all data in a csv file with custom separator using OpenCSV library.
     *
     * @param csvFilePath
     * @throws IOException
     */
    public static void writeAllDataAtOnceToCSVFileWithCustomSeparatorUsingOpenCSV(final String csvFilePath) throws IOException {
        File file = new File(csvFilePath);
        FileWriter outputFile = new FileWriter(file);
        CSVWriter writer = new CSVWriter(outputFile, WRITE_CUSTOM_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);

        writer.writeAll(DATA);
        writer.close();
    }
}
