package com.filehandling.util;

public class Constants {

    private Constants() {

    }

    public static final String REGEX = "\\d{1,}";
    public static final String FOLDER_PATH = "TestFolder";
    public static final String TEST_CONTENT = "Test Content";
    public static final String DELETE_FOLDER_PATH = "TestFolder1";
    public static final String READ_DEMO_FILE_PATH = "TestFile.txt";
    public static final String JAVA_NIO_WRITE_FILE_PATH = "WriteTestFile1";
    public static final String FILE_WRITER_WRITE_FILE_PATH = "WriteTestFile2";
    public static final String MULTIPLE_FILES_DEMO_FILE_PATH = "TestFile1.txt";
    public static final String BUFFERED_WRITER_WRITE_FILE_PATH = "WriteTestFile3";
    public static final String PRINT_LINES_FROM_FILE_TITLE = "Print lines from file";
    public static final String GOOGLE_GUAVA_WRITER_WRITE_FILE_PATH = "WriteTestFile6";
    public static final String READ_ALL_BYTES_FROM_FILE_TITLE = "Read all bytes from file";
    public static final String APACHE_COMMONS_IO_WRITER_WRITE_FILE_PATH = "WriteTestFile5";
    public static final String FILE_OUTPUT_STREAM_WRITER_WRITE_FILE_PATH = "WriteTestFile4";
    public static final String LIST_ALL_FILES_RECURSIVELY_TITLE = "List all files recursively";
    public static final String READ_FILE_USING_GOOGLE_GUAVA_TITLE = "Read file using Google Guava";
    public static final String READ_FILE_USING_INPUT_STREAM_TITLE = "Read file using Input Stream";
    public static final String PRINT_FILTERED_LINES_FROM_FILE_TITLE = "Print filtered lines from file";
    public static final String READ_FILE_USING_BUFFERED_READER_TITLE = "Read file using Buffered Reader";
    public static final String READ_FILE_USING_APACHE_COMMONS_IO_TITLE = "Read file using Apache Commons IO";
    public static final String LIST_ALL_FILES_AND_SUBDIRECTORIES_TITLE = "List all files and subDirectories";

    public static final String PRINT_FILTERED_LINES_FROM_FILE_USING_REGEX_TITLE = "Print filtered lines from file using regex";

    public static final String PRINT_DELIMITER = "------------------%s------------------";
    public static final String COULD_NOT_READ_FILE_ERROR_MESSAGE = "Could not read file %s";
    public static final String READING_LINES_FROM_FILE_MESSAGE = "Reading line %d from file: %s";
    public static final String FILE_NOT_FOUND_AT_PATH_ERROR_MESSAGE = "File not found at path %s";
    public static final String COULD_NOT_WRITE_TO_FILE_ERROR_MESSAGE = "Could not write to file %s";
    public static final String READING_ALL_BYTES_FROM_FILE_MESSAGE = "Reading a file by reading all bytes from it:\n%s";

    public static final String ZIP_FILE_PATH = "TestFile.zip";
    public static final String GZ_FILE_PATH = "TestFile.txt.gz";
    public static final String FOLDER_ZIP_PATH = "FolderZip.zip";
    public static final String TAR_GZ_FILE_PATH = "TestFile.tar.gz";
    public static final String SINGLE_FILE_ZIP_PATH = "SingleFileZip.zip";
    public static final String UNZIPPED_GZ_FILE_PATH = "UnzippedGzFile.txt";
    public static final String UNZIPPED_ZIP_FILE_PATH = "UnzippedZipFile.txt";
    public static final String MULTIPLE_FILES_ZIP_PATH = "MultipleFilesZip.zip";
    public static final String UNZIPPED_TAR_GZ_FILE_PATH = "UnzippedTarGzFile.txt";

    public static final String ORC_FILE_TO_WRITE_PATH = "OrcFileToWrite.orc";
    public static final String COULD_NOT_WRITE_ORC_FILE_TO_PATH = "Couldn't write orc file to path: %s";
    public static final String PRINT_ORC_VALUES = "Byte Column value = %s , Long Column Value %d , Double Column Value = %d";

    public static final String TAB_SEPARATOR = "\t";
    public static final char READ_CUSTOM_SEPARATOR = ';';
    public static final char WRITE_CUSTOM_SEPARATOR = '|';
    public static final String CSV_READ_TEST_FILE_PATH = "CSVTestFile.csv";
    public static final String CSV_WRITE_ALL_DATA_TEST_FILE_PATH = "CSVWriteAllDataTestFile.csv";
    public static final String CSV_WRITE_LINE_BY_LINE_TEST_FILE_PATH = "CSVWriteLineByLineTestFile.csv";
    public static final String READING_ALL_DATA_FROM_CSV_FILE_TITLE = "Reading all data from a csv file";
    public static final String CUSTOM_SEPARATOR_CSV_READ_TEST_FILE_PATH = "CustomSeparatorCSVTestFile.csv";
    public static final String CUSTOM_SEPARATOR_CSV_WRITE_TEST_FILE_PATH = "CustomSeparatorCSVWriteTestFile.csv";
    public static final String READING_CSV_FILE_LINE_BY_LINE_TITLE = "Reading a csv file line by line using OpenCSV library";
    public static final String READING_ALL_DATA_FROM_CSV_FILE_USING_CUSTOM_SEPARATOR_TITLE = "Reading all data from a csv file using custom separator";


}
