package com.filehandling.util;

import static org.apache.commons.io.IOUtils.copy;

import net.lingala.zip4j.ZipFile;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ZipUnzipUtil {

    private ZipUnzipUtil() {

    }

    /**
     * Unzip gz file using Apache Commons Compress library.
     *
     * @param zipFile
     * @throws IOException
     */
    public static void unzipGz(final File zipFile, final String targetFilePath) throws IOException {
        GzipCompressorInputStream in = new GzipCompressorInputStream(new FileInputStream(zipFile));
        Path unzippedFilePath = Paths.get(targetFilePath);
        copy(in, new FileOutputStream(unzippedFilePath.toFile()));
    }

    /**
     * Unzip tar.gz file using Apache Commons Compress library.
     *
     * @param zipFile
     * @throws IOException
     */
    public static void unzipTarGz(final File zipFile, final String targetFilePath) throws IOException {
        TarArchiveInputStream fin = new TarArchiveInputStream(new GzipCompressorInputStream(new FileInputStream(zipFile)));
        TarArchiveEntry entry;
        Path unzippedFilePath = null;

        while ((entry = fin.getNextTarEntry()) != null) {
            unzippedFilePath = Paths.get(targetFilePath);
            copy(fin, new FileOutputStream(unzippedFilePath.toFile()));
        }
    }

    /**
     * Unzip a zip file using zip4j library.
     *
     * @param source
     * @param target
     * @throws IOException
     */
    public static void unzipZipFile(final Path source, final Path target) throws IOException {
        new ZipFile(source.toFile()).extractAll(target.toString());
    }

    /**
     * Compress a single file to a zip file using zip4j library.
     *
     * @param zipFilePath
     * @param filePath
     * @throws IOException
     */
    public static void zipSingleFile(String zipFilePath, String filePath) throws IOException {
        new ZipFile(zipFilePath).addFile(filePath);
    }

    /**
     * Compress multiple files to a zip file using zip4j library.
     *
     * @param zipFilePath
     * @param files
     * @throws IOException
     */
    public static void zipMultipleFiles(String zipFilePath, File... files) throws IOException {
        List<File> filesList = new ArrayList<>();

        for (File file : files) {
            filesList.add(file);
        }

        new ZipFile(zipFilePath).addFiles(filesList);
    }

    /**
     * Compress a folder to a zip file using zip4j library.
     *
     * @param zipFilePath
     * @param folderPath
     * @throws IOException
     */
    public static void zipFolder(String zipFilePath, String folderPath) throws IOException {
        new ZipFile(zipFilePath).addFolder(new File(folderPath));
    }
}
