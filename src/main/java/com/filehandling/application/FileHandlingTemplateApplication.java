package com.filehandling.application;

import static com.filehandling.util.Constants.*;

import com.filehandling.service.CSVService;
import com.filehandling.service.FileHandlingService;
import com.filehandling.service.OrcService;
import com.filehandling.service.ZipUnzipService;
import com.opencsv.exceptions.CsvException;

import java.io.IOException;

public class FileHandlingTemplateApplication {

    private static final FileHandlingService FILE_HANDLING_SERVICE = new FileHandlingService();
    private static final ZipUnzipService ZIP_UNZIP_SERVICE = new ZipUnzipService();
    private static final OrcService ORC_SERVICE = new OrcService();
    private static final CSVService CSV_SERVICE = new CSVService();

    public static void main(String[] args) throws IOException, CsvException {
        FILE_HANDLING_SERVICE.readFileAndFolderDemo(READ_DEMO_FILE_PATH, FOLDER_PATH);
        FILE_HANDLING_SERVICE.writeContentToFileDemo();
        FILE_HANDLING_SERVICE.deleteDirectoryDemo(DELETE_FOLDER_PATH);

        ZIP_UNZIP_SERVICE.unzipArchiveFilesDemo();
        ZIP_UNZIP_SERVICE.zipFilesDemo();
        CSV_SERVICE.readCSVFileDemo();
        CSV_SERVICE.writeDataToCSVFileDemo();
    }
}
